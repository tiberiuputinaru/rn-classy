import React, {Component} from 'react'
import {Text,StyleSheet, View,Button } from 'react-native'
import Constants from "expo-constants";
//import RNMusicPicker from 'react-native-music-picker'
//import RNFetchBlob from 'rn-fetch-blob'


const { manifest } = Constants;

const uri = `http://${manifest.debuggerHost.split(':').shift()}:4000`;



export default class Server extends Component {
    constructor(props) {
        super(props)

        this.state = {
            response:"Click to connect to server"
        }
    }
        connect = () => {
            //const URL = "http://192.168.0.101:19000:8080/"
            const URL = `http://${manifest.debuggerHost.split(':').shift()}:8080`;
            fetch(URL).then(response => {
                if(response.status == 200) {
                    return response
                }
                else {
                    throw new Error("Something went wrong")
                }
            }).then(responseText => {
                this.setState({response: responseText})
            }).catch(error => {
                console.error(error.message)
            })
        }
    
    render() {
        return (
            <View>
                <Text style ={styles.title}>{this.state.response}</Text>
                <Button title="connect" onPress={this.connect}></Button>
            </View>
        )
    }
}

// const res = await DocumentPicker.pick({
//     type: [DocumentPicker.types.allFiles],
// });
// this.setState({ singleFile: res });

// const data = new FormData();
// data.append('name', 'Song Upload');
// data.append('file_attachment', fileToUpload);

// let uploadImage = async () => {
//     //Check if any file is selected or not
//     if (singleFile != null) {
//       //If file selected then create FormData
//       const fileToUpload = singleFile;
//       const data = new FormData();
//       data.append('name', 'Song Upload');
//       data.append('file_attachment', fileToUpload);
//       const URL = `http://${manifest.debuggerHost.split(':').shift()}:8080`;
//       let res = await fetch(
//         URL,
//         {
//           method: 'post',
//           body: data,
//           headers: {
//             'Content-Type': 'multipart/form-data; ',
//           },
//         }
//       );
//       let responseJson = await res.json();
//       if (responseJson.status == 1) {
//         alert('Upload Successful');
//       }
//     } else {
//       //if no file selected the show alert
//       alert('Please Select File first');
//     }
// };

const styles = StyleSheet.create({
    title: {
        fontSize: 20,
        margin: 10,
        textAlign:'center'
    }
})
