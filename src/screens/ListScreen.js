import React from 'react';
import {View,Text,StyleSheet,FlatList} from 'react-native';

const ListScreen = () => {
    // const friends = [
    //     {name:'Sergiu', key:'1'},
    //     {name:'Mihai',key:'2'},
    //     {name:'Matei',key:'3'},
    //     {name:'Ples',key:'4'},
    //     {name:'Andrei',key:'5'},
    // ];
    const friends = [
        {name:'Sergiu',age:'22'},
        {name:'Mihai',age:'22'},
        {name:'Matei',age:'22'},
        {name:'Ples',age:'22'},
        {name:'Andrei',age:'22'},
    ];
    //daca nu puneam cheie ne stergea elementele toate atunci cand stergeam doar unul
    // renderItem= functie care va fi apelata pt fiecare elem al arrayului
    return (
    <FlatList
        //horizontal //vedem lista afisata de la stg la dreapta\
        //showsHorizontalScrollIndicator= {false}//nu mai arata bara
        keyExtractor={(friend) => friend.name}//spune ca numele este cheia
        data= {friends} 
        renderItem={({item}) => {
            //element ==={ item: {name:'Friend #1},index:0}
            //item ==={name:"Friend #1"}
        return <Text style={styles.textStyle}>{item.name}- Age {item.age}</Text>;
        }}
        /> 
    );


};

const styles = StyleSheet.create({
    textStyle: {
        marginVertical: 50
    }
    //pune spatii intre elemente
});

export default ListScreen;