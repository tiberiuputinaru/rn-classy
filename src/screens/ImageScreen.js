import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import ImageDetail from '../components/ImageDetail'

const ImageScreen= () => {
    return <View>
         {/* title e un prop facut de noi */}
        <ImageDetail title="Forest" 
        imageSource={require('../../assets/forest.jpg')} 
        score={6}/>
        {/* transmitem de la imgscreen la imgdetail */}
        <ImageDetail title ="Beach" 
        imageSource={require('../../assets/beach.jpg')} 
        score={9}/>
        <ImageDetail title="Mountain" 
        imageSource={require('../../assets/mountain.jpg')} 
        score={8}/>

    </View>
};

const styles = StyleSheet.create({});

export default ImageScreen;

