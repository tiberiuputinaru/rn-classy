//creem un react component
import React from 'react'; //importam tot din react
import {Text, StyleSheet,View} from 'react-native';// importam doar Text si StyleSheet din react-native
//Text e o 'primitiva' react element,utilizata pt a afisa ceva basic
// mai e si Button tot o primitva-care afiseaza un buton
const ComponentsScreen = () => {
    const number = [123,456];
    const color = { color:'red'}// nu ar merge sa folosim acest obiect JS
    const text = <Text>Buna din nouu</Text>// asignam JSX elements unei variabile pe care putem sa o folosim in acest block JSX
    const name = <Text>Tiberiu</Text>
    const greeting = 'Hi salut !'// pot sa declar aici variabile JS
    return (
    <View>
        {/* <Text style ={styles.textStyle}>This is the components screen</Text>
        <Text>Saluut!</Text>
        <Text>{greeting}</Text>
        {text}  */}
        <Text style={styles.textStyleex}>Getting started with React Native</Text>
        <Text style={styles.textStyleex1}>My name is {name}</Text>
        
        
    </View>
    );
    //style e un prop
    //return <Text style ={{fontSize: 30}}>This is the components screen</Text>;//merge si asa doar ca aici nu se mai face validarea

};// returneaza JSX care e un fel de HTML
// cineva face in spate transformarea din ce scriem noi aici in jvascript(Babel)

const styles = StyleSheet.create({
    textStyle:{
        fontSize: 30
    },
    textStyleex:{
        fontSize:45
    },
    textStyleex1:{
        fontSize:20
    }


});
// aceasta functie face si o validare daca scriem de ex textStyle gresit

export default ComponentsScreen;


//reguli JSX
//putem folosi JSX ca un html normal