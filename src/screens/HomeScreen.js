import React from "react";
import { Text, StyleSheet,View,Button, TouchableOpacity} from "react-native";

const HomeScreen = ({navigation}) => {
  //luam deci doar navigation din props
//const HomeScreen = (props) => {
  //props e un obiect
  //console.log(props.navigation)
  return <View>
  <Text style={styles.text}>HomeScreen22</Text>
  <Button 
  //onPress={()=> props.navigation.navigate('Components')}
  onPress={()=> navigation.navigate('Components')}
  title="Go to Components Demo"
  />
  <Button
    title="Go to List Demo"
    //onPress={() => props.navigation.navigate('List')}
    onPress={() => navigation.navigate('List')}
    />
    <Button
    title="Go to Image Demo"
    //onPress={() => props.navigation.navigate('List')}
    onPress={() => navigation.navigate('Image')}
    />
  {/* <TouchableOpacity onPress={()=> props.navigation.navigate('List')}>
    <Text>Go to List Demo</Text>
    <Text>Go to List Demo</Text>
    <Text>Go to List Demo</Text>
  </TouchableOpacity> */}
  </View>
};
//touchable opacity e tot un buton care arata altfel si lumineaza mai bine si poti sa pui mai multe chestii in el

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default HomeScreen;

