import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from "./src/screens/HomeScreen";
import ComponentsScreen from "./src/screens/ComponentsScreen";
import ListScreen from "./src/screens/ListScreen"
import ImageScreen from "./src/screens/ImageScreen"
import Server from "./Server.js"
import Upload from "./src/screens/Upload.js"
const navigator = createStackNavigator(
  {
    Home: HomeScreen,
    Components:ComponentsScreen,
    List: ListScreen,
    Image: ImageScreen,
    ServerJs: Server,
    UploadFile:Upload
  },
  {
    initialRouteName: 'UploadFile',// ce ne afiseaza cand deschidem aplicatia
    defaultNavigationOptions: {
      title: "App"
    }
  }
);

export default createAppContainer(navigator);
